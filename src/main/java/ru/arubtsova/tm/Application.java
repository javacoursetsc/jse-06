package ru.arubtsova.tm;

import ru.arubtsova.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("*** WELCOME TO TASK-MANAGER ***");
        if (parseArgs(args)) System.exit(0);
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter command:");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_ABOUT:
                showAbout();
                break;
            case TerminalConst.CMD_VERSION:
                showVersion();
                break;
            case TerminalConst.CMD_HELP:
                showHelp();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showIncorrectCommand();
        }
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found");
    }

    public static boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        parseArg(arg);
        return true;
    }

    public static void showAbout() {
        System.out.println("About:");
        System.out.println("Name: Anastasia Rubtsova");
        System.out.println("E-mail: Lafontana@mail.ru");
        System.out.println("Company: TSC");
    }

    public static void showVersion() {
        System.out.println("Version: 1.1.0");
    }

    public static void exit() {
        System.exit(0);
    }

    public static void showHelp() {
        System.out.println("Help:");
        System.out.println(TerminalConst.CMD_ABOUT + " - show developer info.");
        System.out.println(TerminalConst.CMD_VERSION + " - show application version.");
        System.out.println(TerminalConst.CMD_HELP + " - show terminal commands.");
        System.out.println(TerminalConst.CMD_EXIT + " - close application.");
    }

}
